<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="two-column-gallery">
	
	<?php $i = 0; ?>
	<?php foreach($rows as $id => $row): ?>
		<?php if ($i == 0 || $i % 2 == 0): ?>
			 <div class="photo-row">
				<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .' two-column-gallery-img"';  } ?>>
					<?php print $row; ?>
				</div>
		<?php else: ?>
				<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .' two-column-gallery-img"';  } ?>>
				<?php print $row; ?>
				</div>
			</div>
		<?php endif; ?>
		
		<?php $i = $i + 1; ?>
		
	<?php endforeach; ?>
	
	<?php if($i % 2 == 0): ?>
		</div>
	<?php endif; ?>
</div>
	
